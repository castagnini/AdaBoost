
#include <iostream>
#include <vector>
#include <cmath>

#include "mainwindow.h"
#include "testinput.h"
#include "splitter.h"
#include "regressor.h"
#include "isotonic.h"
#include "adaboost.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    // Set size of the window
         setFixedSize(600, 500);

         // Create and position the button
         m_quit = new QPushButton("Quit", this);
         m_quit->setGeometry(width() -90, height() -40 , 80, 30);
         connect(m_quit, SIGNAL (clicked()), this,SLOT(quitSlot()));

         m_save = new QPushButton("Save", this);
         m_save->setGeometry(10, height() -40 , 80, 30);
         connect(m_save, SIGNAL (clicked()), this,SLOT(saveSlot()));

         plot = new  QCustomPlot (this);
         plot->setGeometry(5,5,width()-10,height()-50);
        this->InitPlotWithTestData();

}

void MainWindow::quitSlot(){
    QApplication::quit();
}

void MainWindow::saveSlot(){

    plot->savePng(QString("/Users/luca/screenshot.png"));
}


void MainWindow::InitPlotWithTestData(){

    long len_data = test_input_len();
    // QCustomPlot needs a QVector.
    QVector<double> x(len_data), y(len_data);
    // My Regressor uses a std::vector instead
    std::vector<double> yvec(len_data),   xvec(len_data);
    std::vector<double> isoyvec(len_data);

    // Creates data from test input
    double minX,minY,maxX,maxY;
    minX=maxX=testX[0];
    minY=maxY=testY[0];
    for (int i=0; i<len_data; ++i)
    {
      x[i] = testX[i];
      y[i] = testY[i];
      yvec[i] = y[i];
      xvec[i] = x[i];

      // finds max and min for the plot ranges
      minX = minX<x[i]?minX:x[i];
      minY = minY<y[i]?minY:y[i];
      maxX = maxX>x[i]?maxX:x[i];
      maxY = maxY>y[i]?maxY:y[i];
    }

    // calc extra padding for the plots
    double delta = maxX - minX;
    minX -= delta*0.05;
    maxX += delta*0.05;
    delta = maxY - minY;
    minY -= delta*0.05;
    maxY += delta*0.05;

   // minX=450; maxX=480;

    plot->legend->setVisible(true);

    // create graph and assign data to it:
    plot->addGraph();
    plot->graph(0)->setData(x, y);
    plot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 6));
    plot->graph()->setName("Swimming Pace Data");

    // give the axes some labels:
    plot->xAxis->setLabel("days from begin");
    plot->yAxis->setLabel("swim pace for 100m (seconds)");

    // set axes ranges, so we see all data:
    plot->xAxis->setRange(minX, maxX);
    plot->yAxis->setRange(minY, maxY);

    // Calcs splits with a Tree Regressor.
    Regressor<double> tree_regressor(xvec,yvec);
    // defaults to max_depth = 4
    tree_regressor.calc_segments();

    // I don't actually need this to plot them
    tree_regressor.sort_segments();
    // plot each split

    int leg = 1;
    /*
    leg = 1;
    for (Segment<double> & split : tree_regressor.segments){
        addConstToPlot( split.getMean(),x[split.getStart()], x[split.getEnd()-1]);
        if (leg == 1)
            plot->graph()->setName("Decision Tree Regression");
        else
            plot->legend->removeAt(leg);
        leg++;
    }
    */


    leg = 1;
    Isotonic<double> iso(yvec, false);
    iso.isotonic_regression();
    /*
    for (Segment<double> & split : iso.segments){
        addConstToPlot( split.getMean(),x[split.getStart()], x[split.getEnd()-1],Qt::green);
        if (leg == 1)
            plot->graph()->setName("Isotonic Regression");
        else
            plot->legend->removeAt(leg);
        leg++;
    }
    */

    std::default_random_engine generator(0);
    AdaBoostRegressor<double> ABR = AdaBoostRegressor<double>(xvec,yvec,generator,10);
    ABR.fit_predict_clean(AdaBoostRegressor<double>::linear , 4);

    leg=1;
    for (Segment<double> & split : ABR.segments){
        addConstToPlot( split.getMean(),x[split.getStart()], x[split.getEnd()-1],Qt::magenta);
        if (leg == 1)
            plot->graph()->setName("Adaptive Boost Regression");
        else
            plot->legend->removeAt(leg);
        leg++;
    }

    plot->replot();
}

void MainWindow::addConstToPlot(double c, double start, double end, Qt::GlobalColor color ){
    // create graph and assign data to it:
    plot->addGraph();
    QVector<double> x(2),y(2);
    x[0] = start ; x[1] = end;
    y[0] = c ; y[1] = c ;
    plot->graph()->setData(x, y);
    QPen p = QPen(color);
    p.setWidth(5);
    plot->graph()->setPen(p);
}

MainWindow::~MainWindow()
{

}
