#ifndef AdaBoostRegressor_H
#define AdaBoostRegressor_H

#include<random>
#include<iostream>
#include<cmath>
#include<limits>

#include "regressor.h"

template <class T>
class AdaBoostRegressor
{
public:
    AdaBoostRegressor(std::vector<T>&x,std::vector<T>&y, std::default_random_engine & generator,int num_estimates = 10)
        :x(x),y(y),generator(generator),num_estimates(num_estimates)
    {

    }

    enum Loss { linear, squared, exponential};
    void fit(Loss loss = linear,int max_depth_regressor=2);
    void predict();
    void clean();
    void fit_predict_clean(Loss loss = linear,int max_depth_regressor=4);
    void make_segments();
    std::vector<Segment<T> > segments;

private:
    std::vector<T> results ;
    std::vector<T>&x;
    std::vector<T>&y;
    std::default_random_engine & generator;
    int num_estimates ;
    std::vector< std::vector<T> > estimators;
    std::vector<T> est_weights;

    void populateTrainingSet(std::vector<T> &setX, std::vector< T >&setY, std::vector< long >&pos,
                             const std::vector<T> &sample_weight);

};

template<class T>
void AdaBoostRegressor<T>::fit_predict_clean(Loss loss ,int max_depth_regressor){
    fit(loss,max_depth_regressor);
    predict();
    clean();
}

template<class T>
void AdaBoostRegressor<T>::clean(){
    results.clear();
    estimators.clear();
    est_weights.clear();
}

template<class T>
void AdaBoostRegressor<T>::populateTrainingSet(std::vector< T >&setX,std::vector< T >&setY,
                                               std::vector< long >&pos,const std::vector<T> &sample_weight){
    long max_elems = y.size();

    setX.resize(max_elems);
    pos.resize(max_elems);
    setY.resize(max_elems);

    struct pt{
        long p; T y; T x;
        bool operator<( const pt & val ) const {
                return p < val.p;
        }
    };

    std::vector<pt> points(max_elems);
    std::vector<T> cumulative_weight(max_elems);
    cumulative_weight[0] = sample_weight[0];
    for (long i =1; i<max_elems; i++){
        cumulative_weight[i] = cumulative_weight[i-1] + sample_weight[i];
    }
    std::uniform_real_distribution<T> distribution(0,cumulative_weight[max_elems-1]);
    for (long i =0; i<max_elems; i++){
        T rnd_pos = distribution(generator);
        typename std::vector<T>::iterator low;
        low=std::lower_bound (cumulative_weight.begin(), cumulative_weight.end(), rnd_pos);
        points[i].p = (low - cumulative_weight.begin());
        points[i].y = y[points[i].p];
        points[i].x = x[points[i].p];
    }

    std::sort(points.begin(),points.end());
    for (long i =0; i<max_elems; i++){
        pos[i] = points[i].p;
        setX[i] = points[i].x;
        setY[i] = points[i].y;
    }
}


/*
 * Inspired from AdaBoostRegressor from scikit-learn. Uses algo from:
 * H. Drucker, "Improving Regressors using Boosting Techniques", 1997.
 *
 */
template<class T>
void AdaBoostRegressor<T>::fit(AdaBoostRegressor<T>::Loss loss, int max_depth_regressor){

    std::vector<T> sample_weight(x.size());
    for (T&t:sample_weight) t=1./(T)x.size();

    for (int i = 0 ; i < num_estimates ; i++){
        std::vector< T > setX(x.size());
        std::vector< T > setY(x.size());
        std::vector< long >pos(x.size());
        populateTrainingSet(setX,setY,pos,sample_weight);
        Regressor<T> regressor(setX,setY,max_depth_regressor);
        regressor.calc_segments();
        regressor.sort_segments();
        regressor.rescale_segments_to_global(pos);

        std::vector<T> prediction(x.size());
        std::vector<T> error_vec(x.size());
        regressor.populatePredictions(prediction,x);

        T maxErr = -1;
        for (unsigned long j = 0; j < x.size() ; j++ ){
            error_vec[j] = fabs (y[j] - prediction[j] );
            maxErr = maxErr > error_vec[j] ? maxErr : error_vec[j] ;
        }

        if (maxErr !=0.) for (T & t : error_vec) t /= maxErr ;
        if (loss == squared) for (T & t : error_vec) t *= t ;
        else if (loss == exponential) for (T & t : error_vec) t = 1.-exp(-t) ;

        T estimator_error = 0. ;
        for (unsigned long j =0 ; j < x.size() ; j++ ){
            estimator_error += error_vec[j] * sample_weight[j];
        }

        if(estimator_error >= 0.5) break;

        T beta = estimator_error / (1. - estimator_error);

        T estimator_weight = log(1. / beta);
        if (estimator_error <= 0) { estimator_weight = 1.;}

        estimators.push_back(prediction);
        est_weights.push_back(estimator_weight);

        if (estimator_error <= 0) break;

        if(i != (num_estimates -1)){
            for (unsigned long j =0 ; j < x.size() ; j++ ){
                sample_weight[j] *= pow(beta,(1. - error_vec[j]));
            }
            T sample_weight_sum = 0. ;
            for (T & t : sample_weight) sample_weight_sum += t ;
            if(sample_weight_sum <= 0.) break;
            for (T & t : sample_weight) t /= sample_weight_sum;
        }
    }
}

template<class T>
void AdaBoostRegressor<T>::predict(){
    results.clear();
    unsigned long len = x.size();
    unsigned long num_pred =  estimators.size();
    results.resize(len);


    struct pt{
        T y; T weight;
        bool operator<( const pt & val ) const {
                return y < val.y;
        }
    };

    std::vector<pt> slice (num_pred);
    std::vector<T> sum_slice (num_pred);

    for(unsigned long i = 0 ; i<len; i++ ){

        for (unsigned long j = 0; j<num_pred ; j ++ ){
            slice[j].y = estimators[j][i] ;
            slice[j].weight = est_weights[j] ;

        }
        std::sort(slice.begin(),slice.end());


        sum_slice[0] = slice[0].weight ;
        for (unsigned long j = 1; j<num_pred ; j ++ ){
            sum_slice[j] = slice[j].weight + sum_slice[j-1];
        }
        T median = 0.5 * sum_slice[num_pred-1];
        unsigned long pos = 0;
        for(pos=0; pos<num_pred; pos++)
        {
            if(sum_slice[pos] >= median) break;
        }
        if (pos == num_pred) pos--;

        results[i] = slice[pos].y;

    }//for i

    Segment<T>::create_segments(results,segments);

}







#endif // AdaBoostRegressor_H
