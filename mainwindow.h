#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include "qcustomplot.h"


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void InitPlotWithTestData();
    ~MainWindow();
    void addConstToPlot(double c,double start, double end,Qt::GlobalColor color = Qt::red);

public slots:
    void quitSlot();
    void saveSlot();

private:
    QPushButton *m_quit, *m_save;
    QPushButton *m_button;

    QCustomPlot *plot;


};

#endif // MAINWINDOW_H
