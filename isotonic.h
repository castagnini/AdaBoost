#ifndef ISOTONIC_H
#define ISOTONIC_H


#include <vector>
#include "splitter.h"

template <class T>
class Isotonic
{

public:
    Isotonic(std::vector<T>&v, bool increasing = true)
    : data(v), increasing(increasing)
    { }

    void isotonic_regression();
    void sort_segments(){
        std::sort( segments.begin(), segments.end() );
    }

    std::vector<Segment<T> > segments;

private:
    std::vector<T> solution ;
    std::vector<T>&data;
    bool increasing;
};


/*
 * This algorithm is taken and simplified from scikit-learn
 * see file _isotonic.pyx
 *
 * Original authors:
 *
    # Authors: Fabian Pedregosa <fabian@fseoane.net>
    #          Alexandre Gramfort <alexandre.gramfort@inria.fr>
    #          Nelle Varoquaux <nelle.varoquaux@gmail.com>
    # License: BSD 3 clause
 *
 */

template <class T>
void Isotonic<T>:: isotonic_regression(){
    long n = data.size();
    solution.resize(n);

    for (long i = 0 ; i<n; i++)
        solution[i] = data[i];

    double maxY= data[0];
    if(!increasing){
        for (long i = 0 ; i<n; i++)
            maxY = maxY>data[i]?maxY:data[i];
        for (long i = 0 ; i<n; i++)
            solution[i] = maxY - solution[i];
    }

    if (n <= 1)
        return ;

    while (true){
        long i = 0;
        bool pooled = false;
        while(i < n){
            long k = i ;
            while (k < n && solution[k] >= solution[k + 1]){
                k += 1;
            }
            if( solution[i] != solution[k] ){
                T num = 0.0 ;
                for (long j = i; j< k+1; j++)
                    num += solution[j] ;

                for (long j = i; j< k+1; j++)
                    solution[j] = num/((double)(k+1-i));
                pooled = true;
             }
             i = k + 1 ;
        }
        if (pooled == false)
            break;
    }

    if(!increasing){
        for (long i = 0 ; i<n; i++)
            solution[i] = maxY - solution[i];
    }
    //create segments
    Segment<T>::create_segments(solution,segments);
    //clear vector
    solution.clear();

}





#endif // ISOTONIC_H

