# AdaBoost

- Implements Decision Tree Regression  of a unidimensional data set.
	```C++
	// the the Y_i points should be already ordered according to X_i
	std::vector<double> Xdata(), Ydata();
	// after data initialization
	Regressor<double> tree_regressor(Xdata,Ydata);
	tree_regressor.calc_segments();
	tree_regressor.sort_segments(); // if we want sorted splits
	// results are in tree_regressor.segments;
    for (Segment<double> & split : tree_regressor.segments){
        std::cout<<"start: " << split.start ;
        std::cout<<" end: " << split.end ;
        std::cout<<" value: " << split.mean << std::endl ;
    }
	```
	![TreeRegressor plot](/regressor.png)

- Implements boosted Decision Tree Regression (Adaptive Boost) 
	```C++
	// the the Y_i points should be already ordered according to X_i
	std::vector<double> Xdata(), Ydata();
	// after data initialization
	 std::default_random_engine generator(0);
    AdaBoostRegressor<double> ABR = AdaBoostRegressor<double>(xvec,yvec,generator);
    ABR.fit_predict_clean(); 

    for (Segment<double> & split : ABR.segments){
    	....
	}
	```
	![AdaBoostRegressor plot](/adaboost.png)


	

- Implements Isotonic Regression of a unidimensional data set.
	```C++
	// some_data contains the Y_i points, they should be already ordered
	// according to X_i
	std::vector<double> some_data();
	// after data initialization
	// bool increasing = false for decreasing functions
	Isotonic<double> iso_regressor(some_data,false);
	tree_regressor.isotonic_regression();
	// results are in tree_regressor.segments;
    for (Segment<double> & split : tree_regressor.segments){
		// do something
    }
	```
	![IsoRegressor plot](/isotonic.png)


- GUI done in Qt using the QCustomPlot library (http://www.qcustomplot.com)
 
