#ifndef REGRESSOR_H
#define REGRESSOR_H

#include <vector>
#include "splitter.h"

template <class T>
class Regressor
{

public:
    Regressor(std::vector<T>&x,std::vector<T>&y, int max_depth = 4)
    : x(x),y(y), splitter(Splitter<T>(x,y)) , max_depth(max_depth)
    { }

    void calc_segments( bool use_tolerance = false ,
                        T tolerance = 0.25 );

    void sort_segments(){
        std::sort( segments.begin(), segments.end() );
    }

    void rescale_segments_to_global(const std::vector<long>&global);

    std::vector<Segment<T> > segments;

    void populatePredictions(std::vector< T >& preds,std::vector< T >& xdata);

private:
    std::vector<T>&y ;
    std::vector<T>&x ;
    Splitter<T> splitter;
    int max_depth;
};

template <class T>
void Regressor<T>:: rescale_segments_to_global(const std::vector<long> &global){
    for( Segment<T> &seg : segments){
        seg.setStart(global[seg.getStart()]);
        seg.setEnd(global[seg.getEnd()-1] +1);
    }
}


template <class T>
void Regressor<T>:: calc_segments (bool use_tolerance   ,
                                   T tolerance  ){

    segments.clear();

    long len_data = x.size();

    Segment<T> first = Segment<T>(0,len_data) ;
    first.calc_mean_mse(y);
    segments.push_back(first);

    T new_mse = first.mse;

    typename Splitter<T>::result split;
    T res =std::numeric_limits<T>::max();

    for (int d = 0 ; (d<max_depth && !use_tolerance) || (use_tolerance && res > tolerance ) ; d++ ){
        long num_segs = segments.size();

        for (long s = 0 ; s<num_segs ; s++){
            if (segments[s].start +1 == segments[s].end) continue;

            split = splitter.find_best_split(segments[s]);
            if(split.split == segments[s].start ) continue;

            new_mse -= segments[s].mse ;

            Segment<T> new_seg = Segment<T>(split.split, segments[s].end,
                    split.mean_right, split.mse_right ) ;
            segments.push_back(new_seg);
            segments[s].end = split.split ;
            segments[s].mean = split.mean_left ;
            segments[s].mse = split.mse_left ;

            new_mse += split.mse_left + split.mse_right;
        }
        res = new_mse/first.mse;
    }
}


template<class T>
void Regressor<T>::populatePredictions(std::vector< T >& preds,std::vector< T >& xdata){

    /*
    long len = xdata.size() ;
    std::vector< T > cuts (segments.size() -1) ;
    std::vector< T > cuts_sorted ;

    for (unsigned long i =0 ; i < segments.size() -1 ; i++ ){
        cuts[i] = x[segments[i].getEnd()];
    }
    cuts_sorted = cuts;
    std::sort(cuts_sorted.begin(),cuts_sorted.end());
    for (unsigned long i =0 ; i < segments.size() -1 ; i++ ){
        if (cuts_sorted[i] != cuts[i]){
            throw "problem with sorting";
        }
    }

    for(long i =0 ; i < len ; i++ ){
        typename std::vector<T>::iterator  up;
        up = std::upper_bound(cuts.begin(),cuts.end(),xdata[i]);
        long pos = up - cuts.begin();
        preds[i] = segments[pos].getMean();
    }
    */

    // faster, but requires rescale_segments_to_global

    unsigned long len = preds.size();

    for( const Segment<T> &elem : segments){
        for (long j = elem.getStart() ; j < elem.getEnd() ; j++ ){
              preds[j] = elem.getMean() ;
        }
    }
    // fills the gaps
     Segment<T> elem =  segments[0];
    for (long h = 0 ; h < elem.getStart() ; h++ ){
        preds[h] = elem.getMean();
    }
    for(unsigned long j = 0 ; j < segments.size() -1; j++){
        elem =  segments[j];
        Segment<T> next =  segments[j+1];
        for (long h = elem.getEnd() ; h < next.getStart() ; h++ ){
              preds[h] = next.getMean();
        }
    }
    elem =  segments[segments.size() -1];
    for (unsigned long h = elem.getEnd() ; h < len ; h++ ){
         preds[h] = elem.getMean();
    }


}

#endif // REGRESSOR_H
