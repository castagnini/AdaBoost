
#ifndef _SPLITTER_H_
#define _SPLITTER_H_

#include <vector>
#include <limits>
#include <cmath>

/* at the moment only double and float are supported */

template<class T> class Splitter;
template<class T> class Regressor;

template<class T>
class Segment {

public:
    Segment (long start, long end,
             T mean=std::numeric_limits<T>::quiet_NaN() ,
             T mse = std::numeric_limits<T>::quiet_NaN()  )
        : start(start), end(end),
        mean(mean), mse( mse)
    {

    }

    void calc_mean_mse(const std::vector<T>&data);

    bool operator<( const Segment<T>& val ) const {
            return start < val.start;
    }

    long getStart() const{
        return start;
    }

    long getEnd() const{
        return end;
    }
    void setStart(long s) {
        start=s;
    }

    void setEnd(long e) {
        end=e;
    }


    T getMean() const{
        return mean;
    }

    friend typename Splitter<T>::result Splitter<T>::find_best_split(const Segment<T> &seg);
    friend void Regressor<T>::calc_segments (bool use_tolerance, T tolerance  );

    static void create_segments(const std::vector<T>&data, std::vector<Segment<T> > &segments);

private:
    long start;
    long end;

    T mean;
    T mse ;

} ;

template<class T>
class Splitter {
public:
    Splitter (std::vector<T>&x,std::vector<T>&y, double prec = 1e-7)
        : x(x),y(y), precision(prec){}

    struct result {
        long split;
        bool do_split;
        T mean_left;
        T mse_right ;
        T mean_right;
        T mse_left ;
    };

    result find_best_split(const Segment<T> &seg);
private:
    std::vector<T>&x ;
    std::vector<T>&y ;
    T precision;

};

template<class T>
void Segment<T>::calc_mean_mse(const std::vector<T> &data){
    mean = 0.;
    mse = 0. ;
    if (start == end) return ;

    mean = 0.;
    for (long split =start; split<end ; split++){
        mean += data[split];
    }
    mean /= (T)(end - start);

    for (long split =start; split<end ; split++){
        mse += (data[split] - mean)*(data[split] - mean) ;
    }
}

template<typename T>
typename Splitter<T>::result Splitter<T>::find_best_split(const Segment<T> &seg){
    Splitter<T>::result  res;
    long start = seg.start;
    long end = seg.end;
    res.do_split=false;
    T min_tot_mse=std::numeric_limits<T>::max();

    res.split = seg.start ;

    long len_right = seg.end - seg.start;
    T sum_x_right = seg.mean *(T)len_right;
    T sum_x2_right = seg.mse + (sum_x_right*sum_x_right) / (T)len_right;
    T sum_x_left =0, sum_x2_left=0;

    long len_left = 0;
    bool include_next=false;
    for (long split =start; split<end -1; split++){
        include_next =(fabs(x[split] - x[split+1])<precision);
        len_left++ ;
        len_right -- ;
        sum_x_left += y[split] ;
        sum_x_right -= y[split] ;
        sum_x2_left += y[split]*y[split] ;
        sum_x2_right -= y[split]*y[split] ;

        T mse_left = sum_x2_left
                - (sum_x_left*sum_x_left)/(T)len_left ;
        T mse_right = sum_x2_right
                - (sum_x_right*sum_x_right)/(T)len_right ;

        T mse_tot = mse_left + mse_right;
        if(mse_tot <  min_tot_mse &&
                !include_next){
            min_tot_mse =  mse_tot;
            res.do_split = true;
            res.split = split+1;
            res.mse_left = mse_left;
            res.mse_right = mse_right;
            res.mean_left = sum_x_left /(T)len_left ;
            res.mean_right = sum_x_right/(T)len_right;
        }
    }

    return res ;
}

template <class T>
void Segment<T>::create_segments(const std::vector<T>&data, std::vector<Segment<T> > &segments){
    segments.clear();
    long n = data.size();
    T current = data[0];
    long start = 0;
    for (long i = 1 ; i<n; i++){
        if(data[i] != current){
            segments.push_back(Segment<T>(start,i,current));
            start = i;
            current = data[i];
        }
    }
    segments.push_back(Segment<T>(start,n,current));
}



#endif // SPLITTER_H

