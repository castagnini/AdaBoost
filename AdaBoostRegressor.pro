#-------------------------------------------------
#
# Project created by QtCreator 2016-03-20T11:15:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = AdaBoostRegressor
TEMPLATE = app
CONFIG += c++11



SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp

HEADERS  += mainwindow.h \
    qcustomplot.h \
    testinput.h \
    splitter.h \
    regressor.h \
    isotonic.h \
    adaboost.h


